package order

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	module "gitlab.com/service31/Data/Module/Order"
)

//OrderDetail entity
type OrderDetail struct {
	gorm.Model
	ProductID         uuid.UUID `gorm:"not null"`
	UnitPrice         float32   `gorm:"not null"`
	Quantity          uint64    `gorm:"not null"`
	IsDiscountUSD     bool      `gorm:"not null;default:false"`
	IsDiscountPercent bool      `gorm:"not null;default:false"`
	DiscountAmount    float32   `gorm:"not null;default:0"`
}

//ToDTO dto
func (o *OrderDetail) ToDTO() *module.OrderDetailDTO {
	dto := &module.OrderDetailDTO{
		ProductID:         o.ProductID.String(),
		UnitPrice:         o.UnitPrice,
		Quantity:          o.Quantity,
		DiscountAmount:    o.DiscountAmount,
		IsDiscountUSD:     o.IsDiscountUSD,
		IsDiscountPercent: o.IsDiscountPercent,
	}
	return dto
}

//MarshalBinary for redis
func (o *OrderDetail) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(o)
}

//UnmarshalBinary for redis
func (o *OrderDetail) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, o)
}
