package order

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	module "gitlab.com/service31/Data/Module/Order"
)

//Address entity
type Address struct {
	gorm.Model
	AddressID    uuid.UUID `gorm:"not null;unique_index"`
	Prefix       string
	Firstname    string
	Lastname     string
	BusinessName string
	IsBusiness   bool   `gorm:"not null;default:false"`
	Address1     string `gorm:"not null"`
	Address2     string
	Zip          string  `gorm:"not null;length:9"`
	PhoneNumber  string  `gorm:"not null"`
	City         string  `gorm:"not null"`
	State        string  `gorm:"not null"`
	Tax          float32 `gorm:"not null;default:0"`
}

//ToDTO dto
func (a *Address) ToDTO() *module.AddressDTO {
	dto := &module.AddressDTO{
		AddressID:    a.AddressID.String(),
		Prefix:       a.Prefix,
		Firstname:    a.Firstname,
		Lastname:     a.Lastname,
		BusinessName: a.BusinessName,
		IsBusiness:   a.IsBusiness,
		Address1:     a.Address1,
		Address2:     a.Address2,
		Zip:          a.Zip,
		PhoneNumber:  a.PhoneNumber,
		City:         a.City,
		State:        a.State,
		Tax:          a.Tax,
	}
	return dto
}

//MarshalBinary for redis
func (a *Address) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(a)
}

//UnmarshalBinary for redis
func (a *Address) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, a)
}
