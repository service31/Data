package order

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	module "gitlab.com/service31/Data/Module/Order"
)

//Business order entity
type Business struct {
	gorm.Model
	BusinessID         uuid.UUID `gorm:"not null;unique_index"`
	Name               string    `grom:"not null"`
	Description        string
	BannerImageURL     string
	LogoURL            string
	DeliveryFee        float32
	IsFixedDeliveryFee bool `gorm:"not null;default:true"`
}

//ToDTO dto
func (b *Business) ToDTO() *module.BusinessDTO {
	dto := &module.BusinessDTO{
		BusinessID:         b.BusinessID.String(),
		Name:               b.Name,
		Description:        b.Description,
		BannerImageURL:     b.BannerImageURL,
		LogoURL:            b.LogoURL,
		DeliveryFee:        b.DeliveryFee,
		IsFixedDeliveryFee: b.IsFixedDeliveryFee,
	}
	return dto
}

//MarshalBinary for redis
func (b *Business) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(b)
}

//UnmarshalBinary for redis
func (b *Business) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, b)
}
