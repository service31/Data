package order

import (
	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Order"
)

//Order entity
type Order struct {
	entity.Base
	EntityID          uuid.UUID `gorm:"not null"`
	ShoppingCartID    uuid.UUID `gorm:"not null"`
	BusinessInfo      Business  `gorm:"not null"`
	PaymentMethod     string
	PaymentID         uuid.UUID `gorm:"not null"`
	IsDiscountUSD     bool      `gorm:"not null;default:false"`
	IsDiscountPercent bool      `gorm:"not null;default:false"`
	DiscountAmount    float32   `gorm:"not null;default:0"`
	Subtotal          float32   `gorm:"not null;default:0"`
	ShippingCost      float32   `gorm:"not null;default:0"`
	Tax               float32   `gorm:"not null;default:0"`
	OrderStatus       string    `gorm:"not null;default:'Placed'"`
	Carrier           string
	TrackingNumber    string

	BillingInfo  Address
	ShippingInfo Address

	Details []*OrderDetail
}

//ToDTO dto
func (o *Order) ToDTO() (*module.OrderDTO, error) {
	val := module.OrderStatus_value[o.OrderStatus]

	dto := &module.OrderDTO{
		ID:                o.ID.String(),
		ShoppingCartID:    o.ShoppingCartID.String(),
		BusinessInfo:      o.BusinessInfo.ToDTO(),
		DiscountAmount:    o.DiscountAmount,
		Subtotal:          o.Subtotal,
		ShippingCost:      o.ShippingCost,
		Tax:               o.Tax,
		PaymentMethod:     o.PaymentMethod,
		PaymentID:         o.PaymentID.String(),
		Status:            module.OrderStatus(val),
		Carrier:           o.Carrier,
		TrackingNumber:    o.TrackingNumber,
		IsDiscountUSD:     o.IsDiscountUSD,
		IsDiscountPercent: o.IsDiscountPercent,
		BillingInfo:       o.BillingInfo.ToDTO(),
		ShippingInfo:      o.ShippingInfo.ToDTO(),
	}

	for _, d := range o.Details {
		dto.OrderDetails = append(dto.OrderDetails, d.ToDTO())
	}
	return dto, nil
}

//MarshalBinary for redis
func (o *Order) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(o)
}

//UnmarshalBinary for redis
func (o *Order) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, o)
}
