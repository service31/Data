package faucet

import (
	"fmt"
	"time"

	"github.com/gogo/protobuf/types"
	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Faucet"
	paymentModule "gitlab.com/service31/Data/Module/Payment"
)

//Player player entity
type Player struct {
	entity.Base
	EntityID uuid.UUID `gorm:"not null"`
	Type     string    `gorm:"not null"`

	LastClaim          time.Time `gorm:"not null"`
	BonusAmountPct     uint32    `gorm:"not null;default:1"`
	ReferalID          uint64    `gorm:"not null;auto_increment;unique_index"`
	ReferalCount       uint64    `gorm:"not null;default:0"`
	ReferalClaimAmount float64   `gorm:"not null;default:0"`

	ReferedByID uuid.UUID

	Histories     []*ClaimHistory
	IPAddressList []*IPAddress
}

//ToDTO dto
func (e *Player) ToDTO() (*module.PlayerDTO, error) {
	t, err := types.TimestampProto(e.LastClaim)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for Player", e.LastClaim)
	}
	tp, ok := paymentModule.CryptoType_value[e.Type]
	if !ok {
		return nil, fmt.Errorf("%s is not found as CryptoType", e.Type)
	}

	dto := &module.PlayerDTO{
		EntityID:           e.EntityID.String(),
		Type:               paymentModule.CryptoType(tp),
		ReferalID:          e.ReferalID,
		LastClaim:          t,
		BonusAmountPct:     e.BonusAmountPct,
		ReferalCount:       e.ReferalCount,
		ReferalClaimAmount: e.ReferalClaimAmount,
	}
	for _, h := range e.Histories {
		hdto, err := h.ToDTO()
		if err != nil {
			return nil, err
		}
		dto.Histories = append(dto.Histories, hdto)
	}
	return dto, nil
}

//MarshalBinary for redis
func (e *Player) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(e)
}

//UnmarshalBinary for redis
func (e *Player) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, e)
}
