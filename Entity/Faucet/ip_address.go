package faucet

import (
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
)

//IPAddress IP Address entity
type IPAddress struct {
	entity.Base
	IPAddress string `gorm:"primary_key"`
	IsBanned  bool   `gorm:"not null;default:false"`

	Players []*Player
}

//MarshalBinary for redis
func (e *IPAddress) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(e)
}

//UnmarshalBinary for redis
func (e *IPAddress) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, e)
}
