package faucet

import (
	"fmt"

	"github.com/gogo/protobuf/types"
	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Faucet"
)

//ClaimHistory claim history entity
type ClaimHistory struct {
	entity.Base
	EntityID uuid.UUID `gorm:"not null"`
	Amount   float64   `gorm:"not null"`
}

//ToDTO dto
func (e *ClaimHistory) ToDTO() (*module.HistoryDTO, error) {
	t, err := types.TimestampProto(e.Base.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for ClaimHistory", e.Base.CreatedAt)
	}
	return &module.HistoryDTO{
		Amount:    e.Amount,
		Timestamp: t,
	}, nil
}

//MarshalBinary for redis
func (e *ClaimHistory) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(e)
}

//UnmarshalBinary for redis
func (e *ClaimHistory) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, e)
}
