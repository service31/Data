package product

import (
	"fmt"

	"github.com/lib/pq"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Product"
)

//Product by entityID
type Product struct {
	entity.Base
	EntityID    uuid.UUID `gorm:"not null;index: entityid"`
	Name        string
	Description string
	Brand       string
	ImageURL    string
	UnitPrice   float32        `gorm:"not null;default: 0"`
	MAP         float32        `gorm:"not null;default: -1"`
	Ingredient  pq.StringArray `gorm:"type:text[]"` //KeyValuePair
	Nutrients   pq.StringArray `gorm:"type:text[]"` //KeyvaluePair
	UPC         string
	SKUNumber   string

	IsActive      bool `gorm:"not null;default:false"`
	CategoryInfo  *Category
	InventoryInfo []*Inventory `gorm:"foreignkey:ProductID"`
}

//MarshalBinary for redis
func (p *Product) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(p)
}

//UnmarshalBinary for redis
func (p *Product) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, p)
}

//ToDTO in protobuf
func (p *Product) ToDTO() (*module.DetailedProductDTO, error) {
	dto := &module.DetailedProductDTO{
		Product: &module.ProductInfoDTO{
			ID:          p.ID.String(),
			Name:        p.Name,
			Description: p.Description,
			BrandName:   p.Brand,
			IsActive:    p.IsActive,
			ImageURL:    p.ImageURL,
			UPC:         p.UPC,
			SKUNumber:   p.SKUNumber,
		},
		Price: &module.PriceDTO{
			UnitPrice: p.UnitPrice,
			MAP:       p.MAP,
		},
	}
	for _, ing := range p.Ingredient {
		temp := &module.KeyValuePair{}
		err := temp.Unmarshal([]byte(ing))
		if err != nil {
			return nil, fmt.Errorf("Failed to parse ingredient %s", ing)
		}
		dto.Product.Ingredient = append(dto.Product.Ingredient, temp)
	}

	for _, nut := range p.Nutrients {
		temp := &module.KeyValuePair{}
		err := temp.Unmarshal([]byte(nut))
		if err != nil {
			return nil, fmt.Errorf("Failed to parse nutrient %s", nut)
		}
		dto.Product.Nutrients = append(dto.Product.Nutrients, temp)
	}

	for _, inv := range p.InventoryInfo {
		dto.Inventory = append(dto.Inventory, inv.ToDTO())
	}

	dto.Product.Category = p.CategoryInfo.ToDTO()

	return dto, nil
}
