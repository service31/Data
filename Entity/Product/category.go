package product

import (
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Product"
)

//Category self referencing
type Category struct {
	entity.Base
	EntityID uuid.UUID `gorm:"not null"`
	Name     string    `gorm:"not null"`
	Parent   *Category
	Child    *Category
}

//ToDTO dto
func (c *Category) ToDTO() *module.CategoryDTO {
	dto := &module.CategoryDTO{
		ID:   c.ID.String(),
		Name: c.Name,
	}

	if c.Parent != nil {
		dto.Parent = c.Parent.ToDTO()
	}

	if c.Child != nil {
		dto.Child = c.Child.ToDTO()
	}
	return dto
}

//MarshalBinary for redis
func (c *Category) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(c)
}

//UnmarshalBinary for redis
func (c *Category) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, c)
}
