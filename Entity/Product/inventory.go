package product

import (
	"strconv"

	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Product"
)

//Inventory product inventory
type Inventory struct {
	entity.Base
	EntityID  uuid.UUID `gorm:"not null"`
	ProductID uuid.UUID `gorm:"not null;index:inventory_product"`
	Warehouse string
	Amount    uint64
}

//MarshalBinary for redis
func (i *Inventory) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(i)
}

//UnmarshalBinary for redis
func (i *Inventory) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, i)
}

//ToKeyValueDTO key value dto
func (i *Inventory) ToKeyValueDTO() *module.KeyValuePair {
	dto := &module.KeyValuePair{
		Name:  i.Warehouse,
		Value: strconv.Itoa(int(i.Amount)),
	}
	return dto
}

//ToDTO dto
func (i *Inventory) ToDTO() *module.InventoryDTO {
	dto := &module.InventoryDTO{
		InventoryID: i.ID.String(),
		Warehouse:   i.Warehouse,
		Amount:      i.Amount,
	}
	return dto
}
