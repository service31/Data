package business

import (
	"fmt"
	"time"

	"github.com/gogo/protobuf/types"
	"github.com/jinzhu/gorm"
	"github.com/vmihailenco/msgpack"
	module "gitlab.com/service31/Data/Module/Business"
)

//OpenSchedule entity
type OpenSchedule struct {
	gorm.Model
	Day      int32     `grom:"not null"`
	IsAllDay bool      `gorm:"not null;default: false"`
	Start    time.Time `gorm:"not null"`
	End      time.Time `gorm:"not null"`

	IsActive bool `gorm:"not null"`
}

//ToDTO dto
func (o *OpenSchedule) ToDTO() (*module.OpenScheduleDTO, error) {
	start, err := types.TimestampProto(o.Start)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for OpenSchedule", o.Start)
	}
	end, err := types.TimestampProto(o.End)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for OpenSchedule", o.End)
	}

	dto := &module.OpenScheduleDTO{
		Day:      module.ScheduleDay(o.Day),
		IsAllDay: o.IsAllDay,
		Start:    start,
		End:      end,
	}
	return dto, nil
}

//MarshalBinary for redis
func (o *OpenSchedule) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(o)
}

//UnmarshalBinary for redis
func (o *OpenSchedule) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, o)
}
