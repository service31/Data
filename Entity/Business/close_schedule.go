package business

import (
	"fmt"
	"time"

	"github.com/gogo/protobuf/types"
	"github.com/jinzhu/gorm"
	"github.com/vmihailenco/msgpack"
	module "gitlab.com/service31/Data/Module/Business"
)

//CloseSchedule business special close dates
type CloseSchedule struct {
	gorm.Model
	Date     time.Time `gorm:"not null;"`
	Start    time.Time
	End      time.Time
	IsAllDay bool `gorm:"not null;default:true"`
	IsActive bool `gorm:"not null;default:true"`
}

//ToDTO dto
func (c *CloseSchedule) ToDTO() (*module.CloseScheduleDTO, error) {
	date, err := types.TimestampProto(c.Date)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for CloseSchedule", c.Date)
	}
	dto := &module.CloseScheduleDTO{
		Date:     date,
		IsActive: c.IsActive,
	}
	return dto, nil
}

//MarshalBinary for redis
func (c *CloseSchedule) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(c)
}

//UnmarshalBinary for redis
func (c *CloseSchedule) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, c)
}
