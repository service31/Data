package business

import (
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Business"
)

//Business entity
type Business struct {
	entity.Base
	EntityID       uuid.UUID `gorm:"not null"`
	AdminUserID    uuid.UUID `gorm:"not null"`
	Name           string    `grom:"not null"`
	Description    string
	BannerImageURL string
	LogoURL        string
	DeliveryFee    float32
	ReferralID     uint64 `gorm:"not null;type:serial"`

	OpenSchedule  []*OpenSchedule
	CloseSchedule []*CloseSchedule

	IsActive           bool `gorm:"not null;default:true"`
	IsFixedDeliveryFee bool `gorm:"not null;default:true"`
}

//ToDTO detailed dto
func (b *Business) ToDTO() (*module.BusinessDetailedDTO, error) {
	dto := &module.BusinessDetailedDTO{
		ID:                 b.ID.String(),
		AdminUserID:        b.AdminUserID.String(),
		Name:               b.Name,
		Description:        b.Description,
		BannerImageURL:     b.BannerImageURL,
		LogoURL:            b.LogoURL,
		DeliveryFee:        b.DeliveryFee,
		ReferralID:         b.ReferralID,
		IsActive:           b.IsActive,
		IsFixedDeliveryFee: b.IsFixedDeliveryFee,
	}

	for _, o := range b.OpenSchedule {
		temp, err := o.ToDTO()
		if err != nil {
			return nil, err
		}
		dto.OpenSchedule = append(dto.OpenSchedule, temp)
	}

	for _, c := range b.CloseSchedule {
		temp, err := c.ToDTO()
		if err != nil {
			return nil, err
		}
		dto.CloseSchedule = append(dto.CloseSchedule, temp)
	}
	return dto, nil
}

//ToSimpleDTO simple dto
func (b *Business) ToSimpleDTO() (*module.BusinessSimpleDTO, error) {
	dto := &module.BusinessSimpleDTO{
		ID:                 b.ID.String(),
		Name:               b.Name,
		Description:        b.Description,
		BannerImageURL:     b.BannerImageURL,
		LogoURL:            b.LogoURL,
		DeliveryFee:        b.DeliveryFee,
		IsFixedDeliveryFee: b.IsFixedDeliveryFee,
	}

	for _, o := range b.OpenSchedule {
		temp, err := o.ToDTO()
		if err != nil {
			return nil, err
		}
		dto.OpenSchedule = append(dto.OpenSchedule, temp)
	}

	for _, c := range b.CloseSchedule {
		temp, err := c.ToDTO()
		if err != nil {
			return nil, err
		}
		dto.CloseSchedule = append(dto.CloseSchedule, temp)
	}
	return dto, nil
}

//MarshalBinary for redis
func (b *Business) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(b)
}

//UnmarshalBinary for redis
func (b *Business) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, b)
}
