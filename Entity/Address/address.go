package address

import (
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	address "gitlab.com/service31/Data/Module/Address"
)

//Address address entity
type Address struct {
	entity.Base
	EntityID     uuid.UUID `gorm:"primary_key;not null"`
	Prefix       string
	Firstname    string
	Lastname     string
	BusinessName string
	IsBusiness   bool   `gorm:"not null;default:false"`
	Address1     string `gorm:"not null"`
	Address2     string
	Zip          string    `gorm:"not null;length:9"`
	PhoneNumber  string    `gorm:"not null"`
	IsDefault    bool      `gorm:"not null;default:false"`
	IsActive     bool      `gorm:"not null;default:true"`
	CityAndState CityState `gorm:"not null"`
}

//ToDTO to dto
func (a *Address) ToDTO() *address.AddressDTO {
	addr := &address.AddressDTO{
		ID:           a.ID.String(),
		Firstname:    a.Firstname,
		Lastname:     a.Lastname,
		BusinessName: a.BusinessName,
		IsBusiness:   a.IsBusiness,
		Address1:     a.Address1,
		Address2:     a.Address2,
		PhoneNumber:  a.PhoneNumber,
		Zip:          a.Zip,
		City:         a.CityAndState.City,
		State:        a.CityAndState.State,
		Tax:          a.CityAndState.Tax,
		IsDefault:    a.IsDefault,
	}
	return addr
}

//MarshalBinary for redis
func (a *Address) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(a)
}

//UnmarshalBinary for redis
func (a *Address) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, a)
}
