package address

import (
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
)

//CityState lookup table
type CityState struct {
	entity.Base
	City  string  `gorm:"not null"`
	State string  `gorm:"not null"`
	Tax   float32 `gorm:"not null;default:0"`
}

//MarshalBinary for redis
func (c *CityState) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(c)
}

//UnmarshalBinary for redis
func (c *CityState) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, c)
}
