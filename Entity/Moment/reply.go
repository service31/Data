package moment

import (
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Moment"
)

//Reply entity
type Reply struct {
	entity.Base
	EntityID  uuid.UUID `gorm:"not null;index:entityID"`
	Message   string    `gorm:"not null"`
	ImageURLs []string  `gorm:"type:text[]"`
	VideoURLs []string  `gorm:"type:text[]"`
}

//MarshalBinary for redis
func (r *Reply) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(r)
}

//UnmarshalBinary for redis
func (r *Reply) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, r)
}

//ToDTO dto
func (r *Reply) ToDTO() *module.ReplyDTO {
	return &module.ReplyDTO{
		ID:        r.ID.String(),
		EntityID:  r.EntityID.String(),
		Message:   r.Message,
		ImageURLs: r.ImageURLs,
		VideoURLs: r.VideoURLs,
	}
}
