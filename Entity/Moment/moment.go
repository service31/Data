package moment

import (
	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Moment"
)

//Moment entity
type Moment struct {
	entity.Base
	EntityID      uuid.UUID `gorm:"not null;index:entityID"`
	Title         string
	Message       string
	ImageURLs     []string    `gorm:"type:text[]"`
	VideoURLs     []string    `gorm:"type:text[]"`
	LikedEntities []uuid.UUID `gorm:"type:UUID[]"`

	Replies []*Reply
}

//MarshalBinary for redis
func (m *Moment) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(m)
}

//UnmarshalBinary for redis
func (m *Moment) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, m)
}

//ToDTO dto
func (m *Moment) ToDTO() *module.MomentDTO {
	var likedEntities []string
	var replies []*module.ReplyDTO
	for _, id := range m.LikedEntities {
		likedEntities = append(likedEntities, id.String())
	}

	for _, r := range m.Replies {
		replies = append(replies, r.ToDTO())
	}
	return &module.MomentDTO{
		ID:            m.ID.String(),
		EntityID:      m.EntityID.String(),
		Title:         m.Title,
		Message:       m.Message,
		ImageURLs:     m.ImageURLs,
		VideoURLs:     m.VideoURLs,
		LikedEntities: likedEntities,
		Replies:       replies,
	}
}
