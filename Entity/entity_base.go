package entity

import (
	"encoding/json"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

//Base gorm base
type Base struct {
	ID             uuid.UUID `gorm:"type:uuid;primary_key;unique_index"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      *time.Time
	LastModifiedBy string
}

// BeforeCreate will set a UUID rather than numeric ID.
func (base *Base) BeforeCreate(scope *gorm.Scope) error {
	id := uuid.NewV4()
	return scope.SetColumn("ID", id)
}

//MarshalBinary for redis
func (base *Base) MarshalBinary() ([]byte, error) {
	return json.Marshal(base)
}

//UnmarshalBinary for redis
func (base *Base) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return json.Unmarshal(data, base)
}
