package user

import entity "gitlab.com/service31/Data/Entity"

//ThirdPartyAuth for google facebook and etc...
type ThirdPartyAuth struct {
	entity.Base
	PartyName string `gorm:"not null"`
	AuthToken string `gorm:"not null"`

	User User
}
