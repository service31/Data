package user

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	user "gitlab.com/service31/Data/Module/User"
)

//User database entity
type User struct {
	entity.Base
	EntityID    string `gorm:"not null"`
	Username    string `gorm:"not null"`
	Password    string `gorm:"not null"`
	Gender      string
	PhoneNumber string
	Email       string
	DOB         time.Time
	Avatar      string
	Bio         string
	Status      string
	ReferralID  uint64 `gorm:"not null;type:serial"`

	IsPrivacyPolicyAgreed       bool `gorm:"not null;default:false"`
	IsPhoneNumberVerified       bool `gorm:"not null;default:false"`
	IsEmailVerified             bool `gorm:"not null;default:false"`
	IsActive                    bool `gorm:"not null;default:true"`
	IsProfilePrivate            bool `gorm:"not null;default:false"`
	IsPhoneNotificationDisabled bool `gorm:"not null;default:false"`
	IsEmailNotificationDisabled bool `gorm:"not null;default:false"`

	ServiceID uuid.UUID `gorm:"not null"`
}

//ToDTO converter
func (u *User) ToDTO() *user.DetailedUserDTO {
	return &user.DetailedUserDTO{
		ID:                          u.Base.ID.String(),
		Username:                    u.Username,
		Gender:                      u.Gender,
		PhoneNumber:                 u.PhoneNumber,
		Email:                       u.Email,
		DOB:                         u.DOB.Format("2006-01-02"),
		Avatar:                      u.Avatar,
		Bio:                         u.Bio,
		Status:                      u.Status,
		IsPrivacyPolicyAgreed:       u.IsPrivacyPolicyAgreed,
		IsProfilePrivate:            u.IsProfilePrivate,
		IsPhoneNotificationDisabled: u.IsPhoneNotificationDisabled,
		IsEmailNotificationDisabled: u.IsEmailNotificationDisabled,
	}
}

//MarshalBinary for redis
func (u *User) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(u)
}

//UnmarshalBinary for redis
func (u *User) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, u)
}

//ToSimpleDTO converter
func (u *User) ToSimpleDTO() *user.UserSimpleDTO {
	return &user.UserSimpleDTO{
		ID:       u.Base.ID.String(),
		Username: u.Username,
		Gender:   u.Gender,
		DOB:      u.DOB.Format("2006-01-02"),
		Avatar:   u.Avatar,
		Bio:      u.Bio,
		Status:   u.Status,
	}
}
