package user

import (
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
)

//Service for service accounts
type Service struct {
	entity.Base
	Username string `gorm:"not null;unique_index"`
	Password string `gorm:"not null"`
	IsActive bool   `gorm:"not null;default:true"`
}

//MarshalBinary for redis
func (s *Service) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(s)
}

//UnmarshalBinary for redis
func (s *Service) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, s)
}
