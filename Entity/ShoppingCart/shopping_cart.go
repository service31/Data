package shoppingcart

import (
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/ShoppingCart"
)

//ShoppingCart entity
type ShoppingCart struct {
	entity.Base
	//EntityID should be user ID.
	EntityID        uuid.UUID `gorm:"not null;index:entity_business"`
	BusinessID      uuid.UUID `gorm:"not null;index:entity_business"`
	ShippingAddress *Address
	BillingAddress  *Address `gorm:"not null"`
	//ShippingMethodID TODO: Need to replace with actual data
	ShippingMethodID uuid.UUID
	Subtotal         float32 `gorm:"not null;default:0"`
	ShippingCost     float32 `gorm:"not null;default:0"`
	Tax              float32 `gorm:"not null;default:0"`

	ShoppingCartItems []*ShoppingCartItem
	PromotionCodes    []*PromotionCode

	PointsRedeem uint64 `gorm:"not null;default:0"`

	IsActive    bool `gorm:"not null;default:true"`
	IsCompleted bool `gorm:"not null;default:false"`
}

//ToDTO dto
func (s *ShoppingCart) ToDTO() (*module.DetailedShoppingCartDTO, error) {
	dto := &module.DetailedShoppingCartDTO{
		ID:               s.ID.String(),
		ShippingAddress:  s.ShippingAddress.ToDTO(),
		BillingAddress:   s.BillingAddress.ToDTO(),
		ShippingMethodID: s.ShippingMethodID.String(),
		Subtotal:         s.Subtotal,
		ShippingCost:     s.ShippingCost,
		Tax:              s.Tax,

		PointsRedeem: s.PointsRedeem,
		IsCompleted:  s.IsCompleted,
	}

	for _, p := range s.ShoppingCartItems {
		dto.Products = append(dto.Products, p.ToDTO())
	}

	for _, p := range s.PromotionCodes {
		temp, err := p.ToDTO()
		if err != nil {
			return nil, err
		}
		dto.PromotionCodes = append(dto.PromotionCodes, temp)
	}
	return dto, nil
}

//MarshalBinary for redis
func (s *ShoppingCart) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(s)
}

//UnmarshalBinary for redis
func (s *ShoppingCart) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, s)
}
