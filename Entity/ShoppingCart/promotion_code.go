package shoppingcart

import (
	"fmt"
	"time"

	"github.com/gogo/protobuf/types"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/ShoppingCart"
)

//PromotionCode entity
type PromotionCode struct {
	entity.Base
	EntityID  uuid.UUID `gorm:"not null;index:entitycode"`
	Code      string    `gorm:"not null;index:entitycode"`
	IsUSD     bool      `gorm:"not null;default:false"`
	IsPercent bool      `gorm:"not null;default:false"`
	//PercentAmount this will be decimal value. If it's 10% off, this will be saved as 0.1
	DiscountAmount float32   `gorm:"not null;default:0"`
	ActiveFrom     time.Time `gorm:"not null"`
	ActiveTo       time.Time `gorm:"not null"`

	//BusinessID only set when IsBusinessSpecific is true
	BusinessID    uuid.UUID
	LimitedAmount int64 `gorm:"not null;default:-1"`

	IsActive           bool `gorm:"not null;default:true"`
	IsNewCustomerOnly  bool `gorm:"not null;default:false"`
	IsBusinessSpecific bool `gorm:"not null;default:false"`
}

//ToDTO dto
func (p *PromotionCode) ToDTO() (*module.DetailedPromotionCodeDTO, error) {
	from, err := types.TimestampProto(p.ActiveFrom)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for PromotionCodeID: %s", p.ActiveFrom, p.ID.String())
	}
	to, err := types.TimestampProto(p.ActiveTo)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for PromotionCodeID: %s", p.ActiveFrom, p.ID.String())
	}
	dto := &module.DetailedPromotionCodeDTO{
		ID:                 p.ID.String(),
		Code:               p.Code,
		IsUSD:              p.IsUSD,
		IsPercent:          p.IsPercent,
		DiscountAmount:     p.DiscountAmount,
		ActiveFrom:         from,
		ActiveTo:           to,
		BusinessID:         p.BusinessID.String(),
		LimitedAmount:      p.LimitedAmount,
		IsActive:           p.IsActive,
		IsNewCustomerOnly:  p.IsNewCustomerOnly,
		IsBusinessSpecific: p.IsBusinessSpecific,
	}
	return dto, nil
}

//MarshalBinary for redis
func (p *PromotionCode) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(p)
}

//UnmarshalBinary for redis
func (p *PromotionCode) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, p)
}
