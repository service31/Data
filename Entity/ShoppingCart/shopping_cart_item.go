package shoppingcart

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	module "gitlab.com/service31/Data/Module/ShoppingCart"
)

//ShoppingCartItem entity
type ShoppingCartItem struct {
	gorm.Model
	ProductID uuid.UUID `gorm:"not null"`
	Quantity  uint64    `gorm:"not null"`
}

//ToDTO dto
func (s *ShoppingCartItem) ToDTO() *module.ShoppingCartItemDTO {
	dto := &module.ShoppingCartItemDTO{
		ProductID: s.ProductID.String(),
		Quantity:  s.Quantity,
	}
	return dto
}

//MarshalBinary for redis
func (s *ShoppingCartItem) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(s)
}

//UnmarshalBinary for redis
func (s *ShoppingCartItem) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, s)
}
