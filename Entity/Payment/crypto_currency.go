package payment

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Payment"
)

//CryptoCurrency struct
type CryptoCurrency struct {
	entity.Base
	EntityID uuid.UUID        `gorm:"primary_key;not null"`
	Balance  []*CryptoBalance `gorm:"not null"`
}

func (e *CryptoCurrency) ToDTO() (*module.CryptoCurrencyDTO, error) {
	dto := &module.CryptoCurrencyDTO{
		EntityID: e.EntityID.String(),
	}

	for _, b := range e.Balance {
		bdto, err := b.ToDTO()
		if err != nil {
			return nil, fmt.Errorf("Failed to parse CryptoCurrencyDTO. Error: " + err.Error())
		}
		dto.Balance = append(dto.Balance, bdto)
	}
	return dto, nil
}

//MarshalBinary for redis
func (e *CryptoCurrency) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(e)
}

//UnmarshalBinary for redis
func (e *CryptoCurrency) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, e)
}
