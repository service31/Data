package payment

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Payment"
)

//CryptoBalance CryptoBalance entity
type CryptoBalance struct {
	entity.Base
	EntityID uuid.UUID `gorm:"primary_key;not null"`
	Type     string    `gorm:"not null"`
	Balance  float32   `gorm:"not null;default:0"`

	Histories []*CryptoHistory `gorm:"foreignkey:BalanceID"`
}

//ToDTO dto
func (e *CryptoBalance) ToDTO() (*module.CryptoBalanceDTO, error) {
	t, ok := module.CryptoType_value[e.Type]
	if !ok {
		return nil, fmt.Errorf("%s is not found as CryptoType", e.Type)
	}

	dto := &module.CryptoBalanceDTO{
		EntityID: e.EntityID.String(),
		Type:     module.CryptoType(t),
		Balance:  e.Balance,
	}

	for _, h := range e.Histories {
		dto.Histories = append(dto.Histories, h.ToDTO())
	}
	return dto, nil
}

//MarshalBinary for redis
func (e *CryptoBalance) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(e)
}

//UnmarshalBinary for redis
func (e *CryptoBalance) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, e)
}
