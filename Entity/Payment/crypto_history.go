package payment

import (
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Payment"
)

//CryptoHistory transaction history
type CryptoHistory struct {
	entity.Base
	Amount          float32 `gorm:"not null"`
	Reason          string  `gorm:"not null"`
	WithdrawAddress string
	Tx              string
}

//ToDTO dto
func (e *CryptoHistory) ToDTO() *module.CryptoHistoryDTO {
	return &module.CryptoHistoryDTO{
		Amount:          e.Amount,
		Reason:          e.Reason,
		WithdrawAddress: e.WithdrawAddress,
		Tx:              e.Tx,
	}
}

//MarshalBinary for redis
func (e *CryptoHistory) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(e)
}

//UnmarshalBinary for redis
func (e *CryptoHistory) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, e)
}
