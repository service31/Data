package quest

import (
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Quest"
)

//QuestProgress entity
type QuestProgress struct {
	entity.Base
	EntityID        uuid.UUID `gorm:"not null"`
	CurrentProgress float32   `gorm:"not null;default:0"`

	IsCancelled   bool   `gorm:"not null;default:false"`
	IsCompleted   bool   `gorm:"not null;default:false"`
	AcceptedQuest *Quest `gorm:"not null"`
}

//ToDTO dto
func (q *QuestProgress) ToDTO() (*module.QuestProgressDTO, error) {
	questDTO, err := q.AcceptedQuest.ToDTO()
	if err != nil {
		return nil, err
	}
	dto := &module.QuestProgressDTO{
		CurrentProgress: q.CurrentProgress,
		IsCancelled:     q.IsCancelled,
		IsCompleted:     q.IsCompleted,
		Quest:           questDTO,
	}
	return dto, nil
}

//MarshalBinary for redis
func (q *QuestProgress) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(q)
}

//UnmarshalBinary for redis
func (q *QuestProgress) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, q)
}
