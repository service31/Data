package quest

import (
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Quest"
)

//Points entity for point balance for entity
type Points struct {
	entity.Base
	EntityID    uuid.UUID `gorm:"not null;primary_key;unique_index"`
	TotalPoints int64     `gorm:"not null;default:0"`

	History []*PointHistory
}

//ToDTO dto
func (p *Points) ToDTO() *module.PointsDTO {
	dto := &module.PointsDTO{
		TotalPoints: p.TotalPoints,
	}
	return dto
}

//MarshalBinary for redis
func (p *Points) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(p)
}

//UnmarshalBinary for redis
func (p *Points) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, p)
}
