package quest

import (
	"fmt"
	"time"

	"github.com/gogo/protobuf/types"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	entity "gitlab.com/service31/Data/Entity"
	module "gitlab.com/service31/Data/Module/Quest"
)

//Quest entity
type Quest struct {
	entity.Base
	//EntityID service ID
	EntityID uuid.UUID `gorm:"not null"`
	//BusinessID only set when IsBusinessSpecific is true
	BusinessID uuid.UUID
	//UserID only set when IsUserSpecific is true
	UserID    uuid.UUID
	Type      int32 `gorm:"not null"`
	Threshold float32
	//RewardPoints points to reward once completed
	RewardPoints   uint64 `gorm:"not null;default:0"`
	Description    string
	BannerImageURL string
	//AmountOfCompletionPerDay -1 means unlimited
	AmountOfCompletionPerDay uint32 `gorm:"not null;default:1"`

	ActiveFrom time.Time
	ActiveTo   time.Time

	IsActive           bool `gorm:"not null;default:false"`
	IsBusinessQuest    bool `gorm:"not null;default:false"`
	IsBusinessSpecific bool `gorm:"not null;default:false"`
	IsUserSpecific     bool `gorm:"not null;default:false"`
	IsOncePerEntity    bool `gorm:"not null;default:false"`
}

//ToDTO dto
func (q *Quest) ToDTO() (*module.QuestDTO, error) {
	from, err := types.TimestampProto(q.ActiveFrom)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format", q.ActiveFrom)
	}
	to, err := types.TimestampProto(q.ActiveTo)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format", q.ActiveTo)
	}
	dto := &module.QuestDTO{
		ID:                       q.ID.String(),
		Type:                     module.QuestType(q.Type),
		Threshold:                q.Threshold,
		RewardPoints:             q.RewardPoints,
		Description:              q.Description,
		BannerImageURL:           q.BannerImageURL,
		AmountOfCompletionPerDay: q.AmountOfCompletionPerDay,
		ActiveFrom:               from,
		ActiveTo:                 to,
		IsActive:                 q.IsActive,
		IsBusinessQuest:          q.IsBusinessQuest,
		IsBusinessSpecific:       q.IsBusinessQuest,
		IsOncePerEntity:          q.IsOncePerEntity,
	}
	return dto, nil
}

//MarshalBinary for redis
func (q *Quest) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(q)
}

//UnmarshalBinary for redis
func (q *Quest) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, q)
}
