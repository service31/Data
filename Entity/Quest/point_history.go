package quest

import (
	"fmt"

	"github.com/gogo/protobuf/types"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"github.com/vmihailenco/msgpack"
	module "gitlab.com/service31/Data/Module/Quest"
)

//PointHistory entity for quest transaction
type PointHistory struct {
	gorm.Model
	EntityID       uuid.UUID `gorm:"not null"`
	AmountModified int64     `gorm:"not null;default:0"`
	ModifyReason   string
	CompletedQuest *Quest
}

//ToDTO dto
func (p *PointHistory) ToDTO() (*module.PointHistoryDTO, error) {
	quest, err := p.CompletedQuest.ToDTO()
	if err != nil {
		return nil, err
	}
	createTime, err := types.TimestampProto(p.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("Failed to convert %v to timestamp format for PointHistory", p.CreatedAt)
	}
	dto := &module.PointHistoryDTO{
		Date:           createTime,
		ModifyReason:   p.ModifyReason,
		CompletedQuest: quest,
	}
	return dto, nil
}

//MarshalBinary for redis
func (p *PointHistory) MarshalBinary() ([]byte, error) {
	return msgpack.Marshal(p)
}

//UnmarshalBinary for redis
func (p *PointHistory) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return msgpack.Unmarshal(data, p)
}
