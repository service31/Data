// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: Module/Faucet/faucet.proto

package faucet

import (
	context "context"
	encoding_binary "encoding/binary"
	fmt "fmt"
	_ "github.com/gogo/googleapis/google/api"
	_ "github.com/gogo/protobuf/gogoproto"
	proto "github.com/gogo/protobuf/proto"
	types "github.com/gogo/protobuf/types"
	golang_proto "github.com/golang/protobuf/proto"
	_ "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options"
	Payment "gitlab.com/service31/Data/Module/Payment"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = golang_proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type HistoryDTO struct {
	Timestamp            *types.Timestamp `protobuf:"bytes,1,opt,name=Timestamp,proto3" json:"Timestamp,omitempty"`
	Amount               float64          `protobuf:"fixed64,2,opt,name=Amount,proto3" json:"Amount,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *HistoryDTO) Reset()         { *m = HistoryDTO{} }
func (m *HistoryDTO) String() string { return proto.CompactTextString(m) }
func (*HistoryDTO) ProtoMessage()    {}
func (*HistoryDTO) Descriptor() ([]byte, []int) {
	return fileDescriptor_5706e9b2e32da9cd, []int{0}
}
func (m *HistoryDTO) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *HistoryDTO) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_HistoryDTO.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *HistoryDTO) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HistoryDTO.Merge(m, src)
}
func (m *HistoryDTO) XXX_Size() int {
	return m.Size()
}
func (m *HistoryDTO) XXX_DiscardUnknown() {
	xxx_messageInfo_HistoryDTO.DiscardUnknown(m)
}

var xxx_messageInfo_HistoryDTO proto.InternalMessageInfo

func (m *HistoryDTO) GetTimestamp() *types.Timestamp {
	if m != nil {
		return m.Timestamp
	}
	return nil
}

func (m *HistoryDTO) GetAmount() float64 {
	if m != nil {
		return m.Amount
	}
	return 0
}

func (*HistoryDTO) XXX_MessageName() string {
	return "faucet.HistoryDTO"
}

type PlayerDTO struct {
	EntityID             string             `protobuf:"bytes,1,opt,name=EntityID,proto3" json:"EntityID,omitempty"`
	Type                 Payment.CryptoType `protobuf:"varint,2,opt,name=Type,proto3,enum=payment.CryptoType" json:"Type,omitempty"`
	ReferalID            uint64             `protobuf:"varint,3,opt,name=ReferalID,proto3" json:"ReferalID,omitempty"`
	LastClaim            *types.Timestamp   `protobuf:"bytes,11,opt,name=LastClaim,proto3" json:"LastClaim,omitempty"`
	BonusAmountPct       uint32             `protobuf:"varint,12,opt,name=BonusAmountPct,proto3" json:"BonusAmountPct,omitempty"`
	ReferalCount         uint64             `protobuf:"varint,31,opt,name=ReferalCount,proto3" json:"ReferalCount,omitempty"`
	ReferalClaimAmount   float64            `protobuf:"fixed64,32,opt,name=ReferalClaimAmount,proto3" json:"ReferalClaimAmount,omitempty"`
	Histories            []*HistoryDTO      `protobuf:"bytes,51,rep,name=Histories,proto3" json:"Histories,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *PlayerDTO) Reset()         { *m = PlayerDTO{} }
func (m *PlayerDTO) String() string { return proto.CompactTextString(m) }
func (*PlayerDTO) ProtoMessage()    {}
func (*PlayerDTO) Descriptor() ([]byte, []int) {
	return fileDescriptor_5706e9b2e32da9cd, []int{1}
}
func (m *PlayerDTO) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *PlayerDTO) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_PlayerDTO.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *PlayerDTO) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PlayerDTO.Merge(m, src)
}
func (m *PlayerDTO) XXX_Size() int {
	return m.Size()
}
func (m *PlayerDTO) XXX_DiscardUnknown() {
	xxx_messageInfo_PlayerDTO.DiscardUnknown(m)
}

var xxx_messageInfo_PlayerDTO proto.InternalMessageInfo

func (m *PlayerDTO) GetEntityID() string {
	if m != nil {
		return m.EntityID
	}
	return ""
}

func (m *PlayerDTO) GetType() Payment.CryptoType {
	if m != nil {
		return m.Type
	}
	return Payment.CryptoType_BTC
}

func (m *PlayerDTO) GetReferalID() uint64 {
	if m != nil {
		return m.ReferalID
	}
	return 0
}

func (m *PlayerDTO) GetLastClaim() *types.Timestamp {
	if m != nil {
		return m.LastClaim
	}
	return nil
}

func (m *PlayerDTO) GetBonusAmountPct() uint32 {
	if m != nil {
		return m.BonusAmountPct
	}
	return 0
}

func (m *PlayerDTO) GetReferalCount() uint64 {
	if m != nil {
		return m.ReferalCount
	}
	return 0
}

func (m *PlayerDTO) GetReferalClaimAmount() float64 {
	if m != nil {
		return m.ReferalClaimAmount
	}
	return 0
}

func (m *PlayerDTO) GetHistories() []*HistoryDTO {
	if m != nil {
		return m.Histories
	}
	return nil
}

func (*PlayerDTO) XXX_MessageName() string {
	return "faucet.PlayerDTO"
}

type EmptyRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EmptyRequest) Reset()         { *m = EmptyRequest{} }
func (m *EmptyRequest) String() string { return proto.CompactTextString(m) }
func (*EmptyRequest) ProtoMessage()    {}
func (*EmptyRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_5706e9b2e32da9cd, []int{2}
}
func (m *EmptyRequest) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *EmptyRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_EmptyRequest.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *EmptyRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmptyRequest.Merge(m, src)
}
func (m *EmptyRequest) XXX_Size() int {
	return m.Size()
}
func (m *EmptyRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_EmptyRequest.DiscardUnknown(m)
}

var xxx_messageInfo_EmptyRequest proto.InternalMessageInfo

func (*EmptyRequest) XXX_MessageName() string {
	return "faucet.EmptyRequest"
}

type ClaimRequest struct {
	Type                 Payment.CryptoType `protobuf:"varint,1,opt,name=Type,proto3,enum=payment.CryptoType" json:"Type,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *ClaimRequest) Reset()         { *m = ClaimRequest{} }
func (m *ClaimRequest) String() string { return proto.CompactTextString(m) }
func (*ClaimRequest) ProtoMessage()    {}
func (*ClaimRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_5706e9b2e32da9cd, []int{3}
}
func (m *ClaimRequest) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ClaimRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ClaimRequest.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ClaimRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ClaimRequest.Merge(m, src)
}
func (m *ClaimRequest) XXX_Size() int {
	return m.Size()
}
func (m *ClaimRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ClaimRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ClaimRequest proto.InternalMessageInfo

func (m *ClaimRequest) GetType() Payment.CryptoType {
	if m != nil {
		return m.Type
	}
	return Payment.CryptoType_BTC
}

func (*ClaimRequest) XXX_MessageName() string {
	return "faucet.ClaimRequest"
}
func init() {
	proto.RegisterType((*HistoryDTO)(nil), "faucet.HistoryDTO")
	golang_proto.RegisterType((*HistoryDTO)(nil), "faucet.HistoryDTO")
	proto.RegisterType((*PlayerDTO)(nil), "faucet.PlayerDTO")
	golang_proto.RegisterType((*PlayerDTO)(nil), "faucet.PlayerDTO")
	proto.RegisterType((*EmptyRequest)(nil), "faucet.EmptyRequest")
	golang_proto.RegisterType((*EmptyRequest)(nil), "faucet.EmptyRequest")
	proto.RegisterType((*ClaimRequest)(nil), "faucet.ClaimRequest")
	golang_proto.RegisterType((*ClaimRequest)(nil), "faucet.ClaimRequest")
}

func init() { proto.RegisterFile("Module/Faucet/faucet.proto", fileDescriptor_5706e9b2e32da9cd) }
func init() { golang_proto.RegisterFile("Module/Faucet/faucet.proto", fileDescriptor_5706e9b2e32da9cd) }

var fileDescriptor_5706e9b2e32da9cd = []byte{
	// 582 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x53, 0x4d, 0x6f, 0xd3, 0x30,
	0x18, 0x96, 0xbb, 0x0f, 0x11, 0xaf, 0x9b, 0x84, 0xc7, 0x47, 0x14, 0x8d, 0x2e, 0xca, 0x01, 0xaa,
	0xc1, 0xe2, 0xad, 0x95, 0x00, 0xed, 0xd6, 0x8f, 0x01, 0x95, 0xf8, 0xa8, 0xb2, 0x72, 0x45, 0x72,
	0x83, 0xeb, 0x5a, 0x4a, 0xe2, 0x90, 0x38, 0x43, 0xe1, 0xc8, 0x4f, 0x80, 0x9f, 0xc3, 0x85, 0xe3,
	0x8e, 0x48, 0x48, 0x9c, 0x51, 0xc7, 0x0f, 0x41, 0xb1, 0x93, 0x74, 0xa0, 0x4a, 0x3b, 0x39, 0xcf,
	0xeb, 0x27, 0x8f, 0xdf, 0xe7, 0xfd, 0x80, 0xd6, 0x2b, 0xf1, 0x3e, 0x0b, 0x28, 0x7e, 0x46, 0x32,
	0x9f, 0x4a, 0x3c, 0x53, 0x87, 0x1b, 0x27, 0x42, 0x0a, 0xb4, 0xa9, 0x91, 0xb5, 0xc7, 0x84, 0x60,
	0x01, 0xc5, 0x24, 0xe6, 0x98, 0x44, 0x91, 0x90, 0x44, 0x72, 0x11, 0xa5, 0x9a, 0x65, 0xed, 0x97,
	0xb7, 0x0a, 0x4d, 0xb3, 0x19, 0x96, 0x3c, 0xa4, 0xa9, 0x24, 0x61, 0x5c, 0x12, 0x1e, 0xa9, 0xc3,
	0x3f, 0x64, 0x34, 0x3a, 0x4c, 0x3f, 0x12, 0xc6, 0x68, 0x82, 0x45, 0xac, 0x24, 0x56, 0xc8, 0x1d,
	0x32, 0x2e, 0xe7, 0xd9, 0xd4, 0xf5, 0x45, 0x88, 0x99, 0x60, 0x62, 0xa9, 0x5b, 0x20, 0x05, 0xd4,
	0x57, 0x49, 0x7f, 0xcc, 0xb8, 0x0c, 0x88, 0xa6, 0xa7, 0x34, 0x39, 0xe7, 0x3e, 0xed, 0x1e, 0xe3,
	0x21, 0x91, 0x04, 0x97, 0xce, 0xc6, 0x24, 0x0f, 0x69, 0x24, 0x71, 0xac, 0x4f, 0xfd, 0x9f, 0xf3,
	0x0e, 0xc2, 0x17, 0x3c, 0x95, 0x22, 0xc9, 0x87, 0x93, 0x37, 0xe8, 0x29, 0x34, 0x26, 0x55, 0xd6,
	0x26, 0xb0, 0x41, 0x7b, 0xab, 0x63, 0xb9, 0xda, 0x97, 0x5b, 0xbd, 0xef, 0xd6, 0x0c, 0x6f, 0x49,
	0x46, 0x77, 0xe0, 0x66, 0x2f, 0x14, 0x59, 0x24, 0xcd, 0x86, 0x0d, 0xda, 0xc0, 0x2b, 0x91, 0xf3,
	0xab, 0x01, 0x8d, 0x71, 0x40, 0x72, 0x9a, 0x14, 0xfa, 0x16, 0xbc, 0x71, 0x1a, 0x49, 0x2e, 0xf3,
	0xd1, 0x50, 0xc9, 0x1b, 0x5e, 0x8d, 0xd1, 0x03, 0xb8, 0x3e, 0xc9, 0x63, 0xaa, 0xfe, 0xdf, 0xe9,
	0xec, 0xba, 0x55, 0x9e, 0x83, 0x24, 0x8f, 0xa5, 0x28, 0xae, 0x3c, 0x45, 0x40, 0x7b, 0xd0, 0xf0,
	0xe8, 0x8c, 0x26, 0x24, 0x18, 0x0d, 0xcd, 0x35, 0x1b, 0xb4, 0xd7, 0xbd, 0x65, 0xa0, 0xb0, 0xf0,
	0x92, 0xa4, 0x72, 0x10, 0x10, 0x1e, 0x9a, 0x5b, 0xd7, 0x5b, 0xa8, 0xc9, 0xe8, 0x3e, 0xdc, 0xe9,
	0x8b, 0x28, 0x4b, 0x75, 0xe6, 0x63, 0x5f, 0x9a, 0x4d, 0x1b, 0xb4, 0xb7, 0xbd, 0xff, 0xa2, 0xc8,
	0x81, 0xcd, 0xf2, 0xb9, 0x81, 0x32, 0xbc, 0xaf, 0x52, 0xf8, 0x27, 0x86, 0x5c, 0x88, 0x2a, 0x5c,
	0x68, 0x97, 0xa5, 0xb1, 0x55, 0x69, 0x56, 0xdc, 0xa0, 0x23, 0x68, 0xe8, 0x36, 0x70, 0x9a, 0x9a,
	0x5d, 0x7b, 0xad, 0xbd, 0xd5, 0x41, 0x6e, 0x39, 0x84, 0xcb, 0xfe, 0x78, 0x4b, 0x92, 0xb3, 0x03,
	0x9b, 0xa7, 0x61, 0x2c, 0x73, 0x8f, 0x7e, 0xc8, 0x68, 0x2a, 0x9d, 0x27, 0xb0, 0xa9, 0x04, 0x4b,
	0x5c, 0x97, 0x13, 0x5c, 0x53, 0xce, 0xce, 0x37, 0x00, 0xb7, 0xf5, 0xd4, 0x9f, 0xe9, 0xb9, 0x41,
	0x67, 0xd0, 0x78, 0x4e, 0xa5, 0xee, 0x1a, 0xba, 0x55, 0xa5, 0x71, 0xf5, 0x35, 0xeb, 0x66, 0x15,
	0xad, 0x7b, 0xeb, 0xdc, 0xfb, 0xfc, 0xf3, 0xcf, 0xd7, 0xc6, 0x5d, 0x74, 0x5b, 0xed, 0xc7, 0xf9,
	0x71, 0xb9, 0x43, 0x38, 0xd6, 0x3a, 0x6f, 0xe1, 0x86, 0x2e, 0x73, 0x2d, 0x78, 0x35, 0x5d, 0x6b,
	0x65, 0xd4, 0xb1, 0x95, 0xa6, 0xe5, 0xac, 0xd6, 0x3c, 0x01, 0x07, 0x7d, 0xfa, 0xa5, 0xf7, 0x1a,
	0x6d, 0x74, 0xd6, 0x8e, 0xdd, 0xa3, 0x03, 0xd0, 0x48, 0xfa, 0x70, 0x57, 0x7b, 0xb1, 0x99, 0x37,
	0x1e, 0xd8, 0xbd, 0xf1, 0xc8, 0x1e, 0x0a, 0x1f, 0x3d, 0x9c, 0x4b, 0x19, 0xa7, 0x27, 0xb8, 0xda,
	0xeb, 0x72, 0x43, 0x5c, 0x12, 0x73, 0xf7, 0xd3, 0x9c, 0x27, 0x22, 0x62, 0xf3, 0x8c, 0x44, 0xac,
	0x58, 0x9f, 0x8b, 0x45, 0x0b, 0xfc, 0x58, 0xb4, 0xc0, 0xef, 0x45, 0x0b, 0x7c, 0xbf, 0x6c, 0x81,
	0x8b, 0xcb, 0x16, 0x98, 0x6e, 0xaa, 0xd1, 0xe9, 0xfe, 0x0d, 0x00, 0x00, 0xff, 0xff, 0x9f, 0x01,
	0x86, 0x33, 0x27, 0x04, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// FaucetServiceClient is the client API for FaucetService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type FaucetServiceClient interface {
	GetPlayer(ctx context.Context, in *EmptyRequest, opts ...grpc.CallOption) (*PlayerDTO, error)
	Claim(ctx context.Context, in *ClaimRequest, opts ...grpc.CallOption) (*ClaimRequest, error)
}

type faucetServiceClient struct {
	cc *grpc.ClientConn
}

func NewFaucetServiceClient(cc *grpc.ClientConn) FaucetServiceClient {
	return &faucetServiceClient{cc}
}

func (c *faucetServiceClient) GetPlayer(ctx context.Context, in *EmptyRequest, opts ...grpc.CallOption) (*PlayerDTO, error) {
	out := new(PlayerDTO)
	err := c.cc.Invoke(ctx, "/faucet.FaucetService/GetPlayer", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *faucetServiceClient) Claim(ctx context.Context, in *ClaimRequest, opts ...grpc.CallOption) (*ClaimRequest, error) {
	out := new(ClaimRequest)
	err := c.cc.Invoke(ctx, "/faucet.FaucetService/Claim", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FaucetServiceServer is the server API for FaucetService service.
type FaucetServiceServer interface {
	GetPlayer(context.Context, *EmptyRequest) (*PlayerDTO, error)
	Claim(context.Context, *ClaimRequest) (*ClaimRequest, error)
}

// UnimplementedFaucetServiceServer can be embedded to have forward compatible implementations.
type UnimplementedFaucetServiceServer struct {
}

func (*UnimplementedFaucetServiceServer) GetPlayer(ctx context.Context, req *EmptyRequest) (*PlayerDTO, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPlayer not implemented")
}
func (*UnimplementedFaucetServiceServer) Claim(ctx context.Context, req *ClaimRequest) (*ClaimRequest, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Claim not implemented")
}

func RegisterFaucetServiceServer(s *grpc.Server, srv FaucetServiceServer) {
	s.RegisterService(&_FaucetService_serviceDesc, srv)
}

func _FaucetService_GetPlayer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EmptyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FaucetServiceServer).GetPlayer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/faucet.FaucetService/GetPlayer",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FaucetServiceServer).GetPlayer(ctx, req.(*EmptyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FaucetService_Claim_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ClaimRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FaucetServiceServer).Claim(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/faucet.FaucetService/Claim",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FaucetServiceServer).Claim(ctx, req.(*ClaimRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _FaucetService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "faucet.FaucetService",
	HandlerType: (*FaucetServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetPlayer",
			Handler:    _FaucetService_GetPlayer_Handler,
		},
		{
			MethodName: "Claim",
			Handler:    _FaucetService_Claim_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "Module/Faucet/faucet.proto",
}

func (m *HistoryDTO) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *HistoryDTO) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *HistoryDTO) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if m.Amount != 0 {
		i -= 8
		encoding_binary.LittleEndian.PutUint64(dAtA[i:], uint64(math.Float64bits(float64(m.Amount))))
		i--
		dAtA[i] = 0x11
	}
	if m.Timestamp != nil {
		{
			size, err := m.Timestamp.MarshalToSizedBuffer(dAtA[:i])
			if err != nil {
				return 0, err
			}
			i -= size
			i = encodeVarintFaucet(dAtA, i, uint64(size))
		}
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *PlayerDTO) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *PlayerDTO) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *PlayerDTO) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if len(m.Histories) > 0 {
		for iNdEx := len(m.Histories) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.Histories[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintFaucet(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0x3
			i--
			dAtA[i] = 0x9a
		}
	}
	if m.ReferalClaimAmount != 0 {
		i -= 8
		encoding_binary.LittleEndian.PutUint64(dAtA[i:], uint64(math.Float64bits(float64(m.ReferalClaimAmount))))
		i--
		dAtA[i] = 0x2
		i--
		dAtA[i] = 0x81
	}
	if m.ReferalCount != 0 {
		i = encodeVarintFaucet(dAtA, i, uint64(m.ReferalCount))
		i--
		dAtA[i] = 0x1
		i--
		dAtA[i] = 0xf8
	}
	if m.BonusAmountPct != 0 {
		i = encodeVarintFaucet(dAtA, i, uint64(m.BonusAmountPct))
		i--
		dAtA[i] = 0x60
	}
	if m.LastClaim != nil {
		{
			size, err := m.LastClaim.MarshalToSizedBuffer(dAtA[:i])
			if err != nil {
				return 0, err
			}
			i -= size
			i = encodeVarintFaucet(dAtA, i, uint64(size))
		}
		i--
		dAtA[i] = 0x5a
	}
	if m.ReferalID != 0 {
		i = encodeVarintFaucet(dAtA, i, uint64(m.ReferalID))
		i--
		dAtA[i] = 0x18
	}
	if m.Type != 0 {
		i = encodeVarintFaucet(dAtA, i, uint64(m.Type))
		i--
		dAtA[i] = 0x10
	}
	if len(m.EntityID) > 0 {
		i -= len(m.EntityID)
		copy(dAtA[i:], m.EntityID)
		i = encodeVarintFaucet(dAtA, i, uint64(len(m.EntityID)))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *EmptyRequest) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *EmptyRequest) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *EmptyRequest) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	return len(dAtA) - i, nil
}

func (m *ClaimRequest) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ClaimRequest) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ClaimRequest) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if m.Type != 0 {
		i = encodeVarintFaucet(dAtA, i, uint64(m.Type))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func encodeVarintFaucet(dAtA []byte, offset int, v uint64) int {
	offset -= sovFaucet(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *HistoryDTO) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Timestamp != nil {
		l = m.Timestamp.Size()
		n += 1 + l + sovFaucet(uint64(l))
	}
	if m.Amount != 0 {
		n += 9
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *PlayerDTO) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.EntityID)
	if l > 0 {
		n += 1 + l + sovFaucet(uint64(l))
	}
	if m.Type != 0 {
		n += 1 + sovFaucet(uint64(m.Type))
	}
	if m.ReferalID != 0 {
		n += 1 + sovFaucet(uint64(m.ReferalID))
	}
	if m.LastClaim != nil {
		l = m.LastClaim.Size()
		n += 1 + l + sovFaucet(uint64(l))
	}
	if m.BonusAmountPct != 0 {
		n += 1 + sovFaucet(uint64(m.BonusAmountPct))
	}
	if m.ReferalCount != 0 {
		n += 2 + sovFaucet(uint64(m.ReferalCount))
	}
	if m.ReferalClaimAmount != 0 {
		n += 10
	}
	if len(m.Histories) > 0 {
		for _, e := range m.Histories {
			l = e.Size()
			n += 2 + l + sovFaucet(uint64(l))
		}
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *EmptyRequest) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *ClaimRequest) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Type != 0 {
		n += 1 + sovFaucet(uint64(m.Type))
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func sovFaucet(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozFaucet(x uint64) (n int) {
	return sovFaucet(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *HistoryDTO) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowFaucet
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: HistoryDTO: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: HistoryDTO: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Timestamp", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthFaucet
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthFaucet
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.Timestamp == nil {
				m.Timestamp = &types.Timestamp{}
			}
			if err := m.Timestamp.Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 2:
			if wireType != 1 {
				return fmt.Errorf("proto: wrong wireType = %d for field Amount", wireType)
			}
			var v uint64
			if (iNdEx + 8) > l {
				return io.ErrUnexpectedEOF
			}
			v = uint64(encoding_binary.LittleEndian.Uint64(dAtA[iNdEx:]))
			iNdEx += 8
			m.Amount = float64(math.Float64frombits(v))
		default:
			iNdEx = preIndex
			skippy, err := skipFaucet(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *PlayerDTO) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowFaucet
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: PlayerDTO: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: PlayerDTO: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field EntityID", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthFaucet
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthFaucet
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.EntityID = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Type", wireType)
			}
			m.Type = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Type |= Payment.CryptoType(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 3:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field ReferalID", wireType)
			}
			m.ReferalID = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.ReferalID |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 11:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field LastClaim", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthFaucet
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthFaucet
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.LastClaim == nil {
				m.LastClaim = &types.Timestamp{}
			}
			if err := m.LastClaim.Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 12:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field BonusAmountPct", wireType)
			}
			m.BonusAmountPct = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.BonusAmountPct |= uint32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 31:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field ReferalCount", wireType)
			}
			m.ReferalCount = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.ReferalCount |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 32:
			if wireType != 1 {
				return fmt.Errorf("proto: wrong wireType = %d for field ReferalClaimAmount", wireType)
			}
			var v uint64
			if (iNdEx + 8) > l {
				return io.ErrUnexpectedEOF
			}
			v = uint64(encoding_binary.LittleEndian.Uint64(dAtA[iNdEx:]))
			iNdEx += 8
			m.ReferalClaimAmount = float64(math.Float64frombits(v))
		case 51:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Histories", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthFaucet
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthFaucet
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Histories = append(m.Histories, &HistoryDTO{})
			if err := m.Histories[len(m.Histories)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipFaucet(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *EmptyRequest) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowFaucet
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: EmptyRequest: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: EmptyRequest: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		default:
			iNdEx = preIndex
			skippy, err := skipFaucet(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *ClaimRequest) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowFaucet
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ClaimRequest: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ClaimRequest: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Type", wireType)
			}
			m.Type = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Type |= Payment.CryptoType(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipFaucet(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthFaucet
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipFaucet(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowFaucet
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowFaucet
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthFaucet
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupFaucet
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthFaucet
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthFaucet        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowFaucet          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupFaucet = fmt.Errorf("proto: unexpected end of group")
)
