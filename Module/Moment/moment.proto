syntax = "proto3";

package moment;

import "google/protobuf/timestamp.proto";
import "google/api/annotations.proto";
import "protoc-gen-swagger/options/annotations.proto";
import "github.com/gogo/protobuf/gogoproto/gogo.proto";
import "github.com/mwitkow/go-proto-validators/validator.proto";

// Enable custom Marshal method.
option (gogoproto.marshaler_all) = true;
// Enable custom Unmarshal method.
option (gogoproto.unmarshaler_all) = true;
// Enable custom Size method (Required by Marshal and Unmarshal).
option (gogoproto.sizer_all) = true;
// Enable registration with golang/protobuf for the grpc-gateway.
option (gogoproto.goproto_registration) = true;
// Enable generation of XXX_MessageName methods for grpc-go/status.
option (gogoproto.messagename_all) = true;

option (grpc.gateway.protoc_gen_swagger.options.openapiv2_swagger) = {
  info: {
    version: "1.0"
  }
  external_docs: {
    url: "https://moment.service.api.zhironghuang.com"
    description: "Moment gRPC API Doc"
  }
  schemes: HTTPS
};

message Empty{}

message BoolResponse {
  bool IsSuccess = 1;
}

message ReplyDTO {
  string ID = 1;
  string EntityID = 2;
  string Message = 3;
  repeated string ImageURLs = 4;
  repeated string VideoURLs = 5;

  google.protobuf.Timestamp CreatedAt = 51;
}

message RepeatedReplyDTO {
  repeated ReplyDTO Results = 1;
}

message MomentDTO {
  string ID = 1;
  string EntityID = 2;
  string Title = 3;
  string Message = 4;
  repeated string ImageURLs = 5;
  repeated string VideoURLs = 6;
  repeated ReplyDTO Replies = 7;
  repeated string LikedEntities = 8;

  google.protobuf.Timestamp CreatedAt = 51;
}

message RepeatedMomentDTO {
  repeated MomentDTO Results = 1;
}

message CreateMomentRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string Title = 2;
  string Message = 3 [(validator.field) = {regex: "^.+"}];
  repeated string ImageURLs = 4;
  repeated string VideoURLs = 5;
}

message GetAllMomentsByEntityIDRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message GetMomentByIDRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message DeleteMomentByIDRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message CreateReplyRequest {
  string MomentID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string EntityID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string Message = 3;
  repeated string ImageURLs = 4;
  repeated string VideoURLs = 5;
}

message GetReplyByIDRequest {
  string MomentID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string ID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message DeleteReplyByIDRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

service MomentService {
  rpc CreateMoment(CreateMomentRequest) returns (MomentDTO) {
    option (google.api.http) = {
      post: "/api/v1/moment/create"
      body: "*"
    };
  }
  rpc GetAllMomentsByEntityID(GetAllMomentsByEntityIDRequest) returns (RepeatedMomentDTO) {
    option (google.api.http) = {
      get: "/api/v1/moment/getall/{EntityID}"
    };
  }
  rpc GetMomentByID(GetMomentByIDRequest) returns (MomentDTO) {
    option (google.api.http) = {
      get: "/api/v1/moment/get/{ID}"
    };
  }
  rpc DeleteMomentByID(DeleteMomentByIDRequest) returns (BoolResponse) {
    option (google.api.http) = {
      post: "/api/v1/moment/delete"
      body: "*"
    };
  }
  rpc CreateReply(CreateReplyRequest) returns (ReplyDTO) {
    option (google.api.http) = {
      post: "/api/v1/reply/create"
      body: "*"
    };
  }
  rpc GetReplyByID(GetReplyByIDRequest) returns (ReplyDTO) {
    option (google.api.http) = {
      get: "/api/v1/reply/get/{MomentID}/{ID}"
    };
  }
  rpc DeleteReplyByID(DeleteReplyByIDRequest) returns (BoolResponse) {
    option (google.api.http) = {
      post: "/api/v1/reply/delete"
      body: "*"
    };
  }
}