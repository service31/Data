// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: Module/Payment/payment.proto

package payment

import (
	fmt "fmt"
	math "math"
	proto "github.com/gogo/protobuf/proto"
	golang_proto "github.com/golang/protobuf/proto"
	_ "github.com/gogo/googleapis/google/api"
	_ "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options"
	_ "github.com/gogo/protobuf/gogoproto"
	_ "github.com/mwitkow/go-proto-validators"
	regexp "regexp"
	github_com_mwitkow_go_proto_validators "github.com/mwitkow/go-proto-validators"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = golang_proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

func (this *EmptyRequest) Validate() error {
	return nil
}
func (this *CryptoHistoryDTO) Validate() error {
	return nil
}
func (this *CryptoBalanceDTO) Validate() error {
	for _, item := range this.Histories {
		if item != nil {
			if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(item); err != nil {
				return github_com_mwitkow_go_proto_validators.FieldError("Histories", err)
			}
		}
	}
	return nil
}
func (this *CryptoCurrencyDTO) Validate() error {
	for _, item := range this.Balance {
		if item != nil {
			if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(item); err != nil {
				return github_com_mwitkow_go_proto_validators.FieldError("Balance", err)
			}
		}
	}
	return nil
}

var _regex_CreateTxByEntityRequest_Reason = regexp.MustCompile(`^.+$`)

func (this *CreateTxByEntityRequest) Validate() error {
	if !_regex_CreateTxByEntityRequest_Reason.MatchString(this.Reason) {
		return github_com_mwitkow_go_proto_validators.FieldError("Reason", fmt.Errorf(`value '%v' must be a string conforming to regex "^.+$"`, this.Reason))
	}
	return nil
}

var _regex_WithdrawRequest_Address = regexp.MustCompile(`^.+$`)

func (this *WithdrawRequest) Validate() error {
	if !_regex_WithdrawRequest_Address.MatchString(this.Address) {
		return github_com_mwitkow_go_proto_validators.FieldError("Address", fmt.Errorf(`value '%v' must be a string conforming to regex "^.+$"`, this.Address))
	}
	return nil
}
