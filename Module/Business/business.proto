syntax = "proto3";

package business;

import "google/protobuf/timestamp.proto";
import "google/api/annotations.proto";
import "protoc-gen-swagger/options/annotations.proto";
import "github.com/gogo/protobuf/gogoproto/gogo.proto";
import "github.com/mwitkow/go-proto-validators/validator.proto";

// Enable custom Marshal method.
option (gogoproto.marshaler_all) = true;
// Enable custom Unmarshal method.
option (gogoproto.unmarshaler_all) = true;
// Enable custom Size method (Required by Marshal and Unmarshal).
option (gogoproto.sizer_all) = true;
// Enable registration with golang/protobuf for the grpc-gateway.
option (gogoproto.goproto_registration) = true;
// Enable generation of XXX_MessageName methods for grpc-go/status.
option (gogoproto.messagename_all) = true;

option (grpc.gateway.protoc_gen_swagger.options.openapiv2_swagger) = {
  info: {
    version: "1.0"
  }
  external_docs: {
    url: "https://business.service.api.zhironghuang.com"
    description: "Business gRPC API Doc"
  }
  schemes: HTTPS
};

enum ScheduleDay {
  Monday = 0;
  Tuesday = 1;
  Wednesday = 2;
  Thursday = 3;
  Friday = 4;
  Saturday = 5;
  Sunday = 6;
}

message OpenScheduleDTO {
  ScheduleDay Day = 1;
  bool IsAllDay = 2;
  google.protobuf.Timestamp Start = 3;
  google.protobuf.Timestamp End = 4;
}

message CloseScheduleDTO {
  google.protobuf.Timestamp Date = 1;
  bool IsAllDay = 2;
  google.protobuf.Timestamp Start = 3;
  google.protobuf.Timestamp End = 4;
  bool IsActive = 5;
}

message BusinessDetailedDTO {
  string ID = 1;
  string AdminUserID = 2;
  string Name = 3;
  string Description = 4;
  string BannerImageURL = 5;
  string LogoURL = 6;
  float DeliveryFee = 7;
  uint64 ReferralID = 8;

  repeated OpenScheduleDTO OpenSchedule = 21;
  repeated CloseScheduleDTO CloseSchedule = 22;

  bool IsActive = 52;
  bool IsFixedDeliveryFee = 53;
}

message BusinessSimpleDTO {
  string ID = 1;
  string Name = 3;
  string Description = 4;
  string BannerImageURL = 5;
  string LogoURL = 6;
  float DeliveryFee = 7;

  repeated OpenScheduleDTO OpenSchedule = 21;
  repeated CloseScheduleDTO CloseSchedule = 22;
  
  bool IsFixedDeliveryFee = 53;
}

message RepeatedBusinessSimpleDTO {
  repeated BusinessSimpleDTO Results = 1;
}

message Empty{}

message BoolResponse{
  bool IsSuccess = 1;
}

message CreateBusinessRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string AdminUserID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string Name = 3 [(validator.field) = {regex: "^.+$"}];
  string Description = 4;
  string BannerImageURL = 5 [(validator.field) = {regex: "^(http|https):\\\\.+$"}];
  string LogoURL = 6 [(validator.field) = {regex: "^(http|https):\\\\.+$"}];
  float DeliveryFee = 7;

  repeated OpenScheduleDTO OpenSchedule = 21;
  repeated CloseScheduleDTO CloseSchedule = 22;

  bool IsActive = 52;
  bool IsFixedDeliveryFee = 53;
}

message UpdateBusinessRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string AdminUserID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string Name = 3 [(validator.field) = {regex: "^.+$"}];
  string Description = 4;
  string BannerImageURL = 5 [(validator.field) = {regex: "^(http|https):\\\\.+$"}];
  string LogoURL = 6 [(validator.field) = {regex: "^(http|https):\\\\.+$"}];
  float DeliveryFee = 7;
  
  repeated OpenScheduleDTO OpenSchedule = 21;
  repeated CloseScheduleDTO CloseSchedule = 22;

  bool IsActive = 52;
  bool IsFixedDeliveryFee = 53;
}

message GetDetailedBusinessRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message GetBusinessesRequest {
  repeated string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message GetBusinessesByEntityRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message DeleteBusinessRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

service BusinessService {
  rpc Create(CreateBusinessRequest) returns (BusinessDetailedDTO) {
    option (google.api.http) = {
      post: "/api/v1/create"
      body: "*"
    };
  }
  rpc GetDetailedBusiness(GetDetailedBusinessRequest) returns (BusinessDetailedDTO) {
    option (google.api.http) = {
      get: "/api/v1/get/detail/{ID}"
    };
  }
  rpc GetCurrentBusiesses(Empty) returns (RepeatedBusinessSimpleDTO) {
    option (google.api.http) = {
      get: "/api/v1/get/current"
    };
  }
  rpc GetBusinesses(GetBusinessesRequest) returns (RepeatedBusinessSimpleDTO) {
    option (google.api.http) = {
      post: "/api/v1/get/businesses"
      body: "*"
    };
  }
  rpc GetBusinessesByEntity(GetBusinessesByEntityRequest) returns (RepeatedBusinessSimpleDTO) {
    option (google.api.http) = {
      get: "/api/v1/get/byentity/{EntityID}"
    };
  }
  rpc Update(UpdateBusinessRequest) returns (BusinessDetailedDTO) {
    option (google.api.http) = {
      post: "/api/v1/update"
      body: "*"
    };
  }
  rpc Delete(DeleteBusinessRequest) returns (BoolResponse) {
    option (google.api.http) = {
      post: "/api/v1/delete"
      body: "*"
    };
  }
}
