syntax = "proto3";

package shoppingcart;

import "google/protobuf/timestamp.proto";
import "google/api/annotations.proto";
import "protoc-gen-swagger/options/annotations.proto";
import "github.com/gogo/protobuf/gogoproto/gogo.proto";
import "github.com/mwitkow/go-proto-validators/validator.proto";

// Enable custom Marshal method.
option (gogoproto.marshaler_all) = true;
// Enable custom Unmarshal method.
option (gogoproto.unmarshaler_all) = true;
// Enable custom Size method (Required by Marshal and Unmarshal).
option (gogoproto.sizer_all) = true;
// Enable registration with golang/protobuf for the grpc-gateway.
option (gogoproto.goproto_registration) = true;
// Enable generation of XXX_MessageName methods for grpc-go/status.
option (gogoproto.messagename_all) = true;

option (grpc.gateway.protoc_gen_swagger.options.openapiv2_swagger) = {
  info: {
    version: "1.0"
  }
  external_docs: {
    url: "https://cart.service.api.zhironghuang.com"
    description: "ShoppingCart gRPC API Doc"
  }
  schemes: HTTPS
};

message BoolResponse {
  bool IsSuccess = 1;
}

message AddressDTO {
  string AddressID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string Prefix = 2;
  string Firstname = 3 [(validator.field) = {regex: "^\\w+$"}];
  string Lastname = 4  [(validator.field) = {regex: "^\\w+$"}];;
  string BusinessName = 5;
  bool IsBusiness = 6;
  string Address1 = 7 [(validator.field) = {regex: "^\\w+$"}];;
  string Address2 = 8;
  string PhoneNumber = 9;
  string Zip = 10 [(validator.field) = {regex: "^[0-9]{5}(-[0-9]{4})?$"}];;
  string City = 11 [(validator.field) = {regex: "^\\w+$"}];;
  string State = 12 [(validator.field) = {regex: "^\\w+$"}];;
  float Tax = 13;
}

message DetailedPromotionCodeDTO {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string Code = 2;
  bool IsUSD = 3;
  bool IsPercent = 4;
  float DiscountAmount = 5;

  google.protobuf.Timestamp ActiveFrom = 11;
  google.protobuf.Timestamp ActiveTo = 12;

  string BusinessID = 21;
  int64 LimitedAmount = 22;

  bool IsActive = 31;
  bool IsNewCustomerOnly = 32;
  bool IsBusinessSpecific = 33;
}

message RepeatedDetailedPromotionCodeDTO {
  repeated DetailedPromotionCodeDTO Results = 1;
}

message ShoppingCartItemDTO {
  string ProductID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  uint64 Quantity = 2 [(validator.field) = {int_gt: 0}];
}

message DetailedShoppingCartDTO {
  string ID = 1;
  repeated ShoppingCartItemDTO Products = 2;
  AddressDTO ShippingAddress = 3;
  AddressDTO BillingAddress = 4;
  string ShippingMethodID = 5;
  float Subtotal = 6;
  float ShippingCost = 7;
  float Tax = 8;
  
  repeated DetailedPromotionCodeDTO PromotionCodes = 11;
  uint64 PointsRedeem = 12;

  bool IsCompleted = 51;
}


message CreateShoppingCartRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string BusinessID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  AddressDTO ShippingAddress = 3;
  AddressDTO BillingAddress = 4;
  ShoppingCartItemDTO Product = 5;
}

message GetShoppingCartRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string BusinessID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message GetShoppingCartByIDRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message AddProductInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  ShoppingCartItemDTO Product = 2;
}

message RemoveProductInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string ProductID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message ValidatePromoCodesInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message ValidatePromoCodesInShoppingCartResponse {
  DetailedShoppingCartDTO ShoppingCart = 1; 
  repeated string ErrorMessages = 2;
}

message AddPromoCodeInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string PromoCodeID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message RemovePromoCodeInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string PromoCodeID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message UpdateAddressInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string AddressID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];;
  string Prefix = 3;
  string Firstname = 4;
  string Lastname = 5;
  string BusinessName = 6;
  bool IsBusiness = 7;
  string Address1 = 8 [(validator.field) = {regex: "^.+$"}];
  string Address2 = 9;
  string PhoneNumber = 10;
  string Zip = 11 [(validator.field) = {regex: "^\\d{5}(-\\d{4})?$"}];
  string City = 12 [(validator.field) = {regex: "^[a-zA-Z ]+$"}];
  string State = 13 [(validator.field) = {regex: "^[a-zA-Z ]+$"}];
  float Tax = 14;
}

message UpdateShippingMethodInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string ShippingMethodID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message UpdatePointsRedeemInShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  uint64 PointsRedeem = 2;
}

message CompleteShoppingCartReuqest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message MergeShoppingCartRequest {
  string FromID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string ToID = 2 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message DeleteShoppingCartRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message CreatePromoCodeRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string Code = 2 [(validator.field) = {regex: "^[a-zA-Z0-9]{3,}$"}];
  bool IsUSD = 3;
  bool IsPercent = 4;
  float DiscountAmount = 5;

  google.protobuf.Timestamp ActiveFrom = 11;
  google.protobuf.Timestamp ActiveTo = 12;

  string BusinessID = 21;
  int64 LimitedAmount = 22;

  bool IsActive = 31;
  bool IsNewCustomerOnly = 32;
  bool IsBusinessSpecific = 33;
}

message GetAllPromoCodesByEntityRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

message GetPromoCodeByIDRequest {
  string ID = 1;
}

message GetPromoCodeByCodeRequest {
  string EntityID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
  string PromoCode = 2 [(validator.field) = {regex: "^[a-zA-Z0-9]{3,}$"}];
}

message UpdatePromoCodeRequest {
  string ID = 1;
  string Code = 2;
  bool IsUSD = 3;
  bool IsPercent = 4;
  float DiscountAmount = 5;

  google.protobuf.Timestamp ActiveFrom = 11;
  google.protobuf.Timestamp ActiveTo = 12;

  string BusinessID = 21;
  int64 LimitedAmount = 22;

  bool IsActive = 31;
  bool IsNewCustomerOnly = 32;
  bool IsBusinessSpecific = 33;
}

message DeletePromoCodeRequest {
  string ID = 1 [(validator.field) = {regex: "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"}];
}

service ShoppingCartService {
  rpc CreateShoppingCart(CreateShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/create"
      body: "*"
    };
  };
  rpc GetShoppingCart(GetShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      get: "/api/v1/cart/{EntityID}/{BusinessID}"
    };
  };
  rpc GetShoppingCartByID(GetShoppingCartByIDRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      get: "/api/v1/cart/{ID}"
    };
  };
  rpc AddProductInShoppingCart(AddProductInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/add/product"
      body: "*"
    };
  }
  rpc RemoveProductInShoppingCart(RemoveProductInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/remove/product"
      body: "*"
    };
  }
  rpc ValidatePromoCodesInShoppingCart(ValidatePromoCodesInShoppingCartRequest) returns (ValidatePromoCodesInShoppingCartResponse) {
    option (google.api.http) = {
      get: "/api/v1/cart/validate/promo/{ID}"
    };
  }
  rpc AddPromoCodeInShoppingCart(AddPromoCodeInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/add/promocode"
      body: "*"
    };
  }
  rpc RemovePromoCodeInShoppingCart(RemovePromoCodeInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/remove/promocode"
      body: "*"
    };
  }
  rpc UpdateShippingAddressInShoppingCart(UpdateAddressInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/update/shippingaddress"
      body: "*"
    };
  }
  rpc UpdateBillingAddressInShoppingCart(UpdateAddressInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/update/billingaddress"
      body: "*"
    };
  }
  rpc UpdateShippingMethodInShoppingCart(UpdateShippingMethodInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/update/shippingmethod"
      body: "*"
    };
  }
  rpc UpdatePointsRedeemInShoppingCart(UpdatePointsRedeemInShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/update/pointsredeem"
      body: "*"
    };
  }
  rpc CompleteShoppingCart(CompleteShoppingCartReuqest) returns (BoolResponse) {
    option (google.api.http) = {
      post: "/api/v1/cart/complete"
      body: "*"
    };
  }
  rpc MergeShoppingCart(MergeShoppingCartRequest) returns (DetailedShoppingCartDTO) {
    option (google.api.http) = {
      post: "/api/v1/cart/merge"
      body: "*"
    };
  }
  rpc DeleteShoppingCart(DeleteShoppingCartRequest) returns (BoolResponse) {
    option (google.api.http) = {
      post: "/api/v1/cart/delete"
      body: "*"
    };
  }
  rpc CreatePromoCode(CreatePromoCodeRequest) returns (DetailedPromotionCodeDTO) {
    option (google.api.http) = {
      post: "/api/v1/promocode/create"
      body: "*"
    };
  }
  rpc GetAllPromoCodesByEntity(GetAllPromoCodesByEntityRequest) returns (RepeatedDetailedPromotionCodeDTO) {
    option (google.api.http) = {
      get: "/api/v1/promocodes/getall/{EntityID}"
    };
  }
  rpc GetPromoCodeByID(GetPromoCodeByIDRequest) returns (DetailedPromotionCodeDTO) {
    option (google.api.http) = {
      get: "/api/v1/promocode/get/{ID}"
    };
  }
  rpc GetPromoCodeByCode(GetPromoCodeByCodeRequest) returns (DetailedPromotionCodeDTO) {
    option (google.api.http) = {
      post: "/api/v1/promocode/get/code"
      body: "*"
    };
  }
  rpc UpdatePromoCode(UpdatePromoCodeRequest) returns (DetailedPromotionCodeDTO) {
    option (google.api.http) = {
      post: "/api/v1/promocode/update"
      body: "*"
    };
  }
  rpc DeletePromoCode(DeletePromoCodeRequest) returns (BoolResponse) {
    option (google.api.http) = {
      post: "/api/v1/promocode/delete"
      body: "*"
    };
  }
}