# Data

Database entity data and protobuf

Generate pb.go
```
protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Address/address.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Business/business.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src/gitlab.com/service31/Data/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Faucet/faucet.proto 
        

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Moment/moment.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Order/order.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Payment/payment.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Product/product.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/Quest/quest.proto

protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=gogoimport=true:. --gofast_out=plugins=grpc:. ./Module/Redis/redis.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/ShoppingCart/shopping_cart.proto

protoc \
        -I . \
        -I $GOPATH/src/github.com/gogo/protobuf/protobuf/ \
        -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \
        -I $GOPATH/src/github.com/gogo/googleapis/ \
        -I $GOPATH/src \
        --gogo_out=plugins=grpc,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --grpc-gateway_out=allow_patch_feature=false,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        --swagger_out=./ \
        --govalidators_out=gogoimport=true,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api,\
Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/protobuf,\
Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types:./ \
        ./Module/User/user.proto
```

# Deprecated
```
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=gogoimport=true:. --gofast_out=plugins=grpc:. ./Module/Redis/redis.proto
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=gogoimport=true:. --gofast_out=plugins=grpc:. ./Module/User/user.proto
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=gogoimport=true:. --gofast_out=plugins=grpc:. ./Module/Address/address.proto
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=gogoimport=true:. --gofast_out=plugins=grpc:. ./Module/Product/product.proto
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,gogoimport=true:. --gofast_out=Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,plugins=grpc:. ./Module/Business/business.proto
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,gogoimport=true:. --gofast_out=Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,plugins=grpc:. ./Module/ShoppingCart/shopping_cart.proto
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,gogoimport=true:. --gofast_out=Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,plugins=grpc:. ./Module/Quest/quest.proto
protoc -I=. -I=$GOPATH/src/github.com/gogo/protobuf/protobuf -I=$GOPATH/src --govalidators_out=gogoimport=true:. --gofast_out=plugins=grpc:. ./Module/Order/order.proto
```